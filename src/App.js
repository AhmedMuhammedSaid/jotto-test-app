import './App.css';
import GuessedWords from "./GuessedWords";
import Congratulations from "./Congratulations";
function App() {
  return (
    <div className="container">
    <h1>Jotto</h1>
    <Congratulations success={false}/>
    <GuessedWords guessedWords={[{guessedWord:"train",letterMatchCount:4}]} />
    </div>
  );
}

export default App;
