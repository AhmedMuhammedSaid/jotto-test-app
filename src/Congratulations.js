import React from "react";
import propTypes from "prop-types";
/**
 * Functional react component for congratulatory message.
 * @function
 * @returns {JSX.Element} - Rendered component (or null if `success` prop )
 */
const Congratulations = ({ success }) => {
  if (success) {
    return (
      <div data-test="component-congrats" className="alert alert-success">
        <div data-test="congrats-message">
          Congratulations ! You guessed the word!
        </div>
      </div>
    );
  } else {
    return <div data-test="component-congrats" />;
  }
};

Congratulations.propTypes = {
  success: propTypes.bool.isRequired,
};
export default Congratulations;
