import React from "react";
import { shallow } from "enzyme";
import Congrats from "./Congratulations";
import { checkProp, findByAttribute } from "../test/testUtils";

const defaultProps = { success: false };
/*
 *Factory function to create a ShallowWrapper for the App component
 * @function setup
 *@param {object} props - Component props specific to this setup
 *param {object } state - Initial state for setup
 *@returns {shallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Congrats {...setupProps} />);
};

test("render without errors", () => {
  const wrapper = setup({ success: false });
  const component = findByAttribute(wrapper, "component-congrats");
  expect(component.length).toBe(1);
});

test("render no text when `success` prop is false", () => {
  const wrapper = setup({ success: false });
  const component = findByAttribute(wrapper, "component-congrats");
  expect(component.text()).toBe(``);
});
test("render non-empty congrats message  when success prop is true", () => {
  const wrapper = setup({ success: true });
  const message = findByAttribute(wrapper, "congrats-message");
  expect(message.text().length).not.toBe(0);
});

test("doesn't throw warning with expected props", () => {
  const expectedProps = { success: false };
  // first approach
  // const propError = checkPropTypes(
  //   Congrats.propTypes,
  //   expectedProps,
  //   "prop",
  //   Congrats.name
  // );
  // expect(propError).toBeUndefined();

  //second approach
  checkProp(Congrats, expectedProps);
});
