import React from "react";
import { shallow } from "enzyme";
import { findByAttribute, storeFactory } from "../../../test/testUtils";
import Input from "./Input";

/**
 * Factory function to create a ShallowWrapper for the GuessedWords component.
 * @function setup
 * @param {object} initialState - Initial State for the setup
 * @returns {ShallowWrapper}
 */

const setup = (initialState = {}) => {
  const store = storeFactory(initialState);
  const wrapper = shallow(<Input store={store} />).dive();
  //   console.log(wrapper.debug());
  return wrapper;
};

describe("render", () => {
  describe("word has not been guessed", () => {
    let wrapper;
    beforeEach(() => {
      const initialState = { success: false };
      wrapper = setup(initialState);
    });
    test("should render component without error", () => {
      const component = findByAttribute(wrapper, "component-input");
      expect(component.length).toBe(1);
    });
    test("should render input box", () => {
      const inputBox = findByAttribute(wrapper, "input");
      expect(inputBox.length).toBe(1);
    });
    test("should submit button", () => {
      const submissionBtn = findByAttribute(wrapper, "submit-button");
      expect(submissionBtn.length).toBe(1);
    });
  });
  describe("word has been guessed", () => {
    let wrapper;
    beforeEach(() => {
      const initialState = { success: false };
      wrapper = setup(initialState);
    });
    test("should render component without error", () => {
      const component = findByAttribute(wrapper, "component-input");
      expect(component.length).toBe(1);
    });
    test("doesn't  render input box", () => {
      const inputBox = findByAttribute(wrapper, "input");
      expect(inputBox.length).toBe(0);
    });
    test("doesn't render submit button", () => {
      const submissionBtn = findByAttribute(wrapper, "submit-button");
      expect(submissionBtn.length).toBe(0);
    });
  });
});
