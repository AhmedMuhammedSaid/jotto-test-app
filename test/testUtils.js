import checkPropTypes from "check-prop-types";
import { createStore } from "redux";
import rootReducer from "../src/redux/reducers";
/*
 *return Shallow Wrapper containing Nods with the given test-att value
 * @param setup
 *@param {ShallowWrapper} Wrapper - Enzyme wrapper to search within
 *param {string } val - value of test-att attribute for search
 *@returns {ShallowWrapper}
 */
export const findByAttribute = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

/** 
* Create a testing store with imported reducers , middleware ,and initial state 
* globals : rooteReducers
*@param {object} initialState - Initial state for store
*@function storeFactory
@returns {Store } -  Redux Store 
*/
export const storeFactory = (initialState) => {
  return createStore(rootReducer, initialState);
};
export const checkProp = (component, confirmingProps) => {
  const propError = checkPropTypes(
    component.propTypes,
    confirmingProps,
    "prop",
    component.name
  );
  expect(propError).toBeUndefined();
};
